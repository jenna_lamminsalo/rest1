let sanakirja = [
  { suomi: "talo", engl: "house" },
  { suomi: "auto", engl: "car" },
];

const express = require("express");
const fs = require("fs");

var app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

function lueSanakirja() {
  let data = fs.readFileSync("./sanakirja.txt", {
    encoding: "utf8",
    flag: "r",
  });
  data = data.split(/\r?\n/);
  let sanakirja = [];
  for (var i in data) {
    var rivi = data[i];
    if (rivi == "") {
      break;
    }
    var sanat = rivi.split(" ");
    sanakirja.push({
      suomi: sanat[0],
      engl: sanat[1],
    });
  }
  return sanakirja;
}

app.get("/sanakirja", (req, res) => {
  res.json(lueSanakirja());
});

app.post("/sanakirja", (req, res) => {
  const sanapari = req.body;
  const rivi = `${sanapari.suomi} ${sanapari.engl}\r\n`;
  console.log(sanapari, rivi);
  fs.appendFileSync("./sanakirja.txt", rivi);
  res.sendStatus(200);
});

app.get("/sanakirja/:sana", (req, res) => {
  console.log(req.params);
  const sana = req.params.sana;
  const sanakirja = lueSanakirja();
  for (var i in sanakirja) {
    var sanat = sanakirja[i];
    if (sanat.suomi == sana) {
      res.json(sanat);
      return;
    }
  }
  res.sendStatus(404);
});

app.listen(3000);
